from django.apps import AppConfig


class DatascraperConfig(AppConfig):
    name = 'datascraper'
    TWITTER_URL = 'https://twitter.com'
    TWITTER_SUSPENDED_URL = '{0}/account/suspended'.format(TWITTER_URL)
