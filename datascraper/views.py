from rest_framework import viewsets, status
from rest_framework.response import Response
from urllib.error import HTTPError, URLError
from .serializers import TwitterProfileSerializer
from .datascrapers import TwitterDataReader, TwitterDataParser, TwitterDataScraper, AccountSuspendedError


class TwitterProfileViewSet(viewsets.ViewSet):
    serializer_class = TwitterProfileSerializer
    data_source = TwitterDataScraper

    def list(self, request):
        return Response()

    def retrieve(self, request, pk=None):
        try:
            html = TwitterDataReader.read(pk)
            parsed_html = TwitterDataParser.parse(html)
            serializer = self.serializer_class(self.data_source().get_by_username(pk, parsed_html))
            return Response(serializer.data)

        except HTTPError as e:
            if e.code == status.HTTP_404_NOT_FOUND:
                return Response("Not found: {0}".format(pk), status=status.HTTP_404_NOT_FOUND)

        except URLError as e:
            return Response(str(e.reason), status=status.HTTP_404_NOT_FOUND)

        except AccountSuspendedError as e:
            return Response(e.message)

        except Exception:
            return Response('An unexpected error has ocurred', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            