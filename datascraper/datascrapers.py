from urllib.request import urlopen
from bs4 import BeautifulSoup
import html2text
from django.apps import apps
from .models import TwitterProfile


config = apps.get_app_config('datascraper')

class TwitterDataReader():
    def read(username):
        complete_url = "{0}/{1}".format(config.TWITTER_URL, username)
        return urlopen(complete_url).read()

class TwitterDataParser():
    def parse(html):
        return BeautifulSoup(html, "html.parser")

class TwitterDataScraper():
    def get_by_username(self, username, parsed_html):
        self._validate_account(parsed_html)

        return self._build_profile(parsed_html, username.lower())

    def _validate_account(self, parsed_html):
        if parsed_html.find('link', {'href':config.TWITTER_SUSPENDED_URL}) is not None:
            raise AccountSuspendedError()

    def _build_profile(self, parsed_html, username):
        name = self._get_name(parsed_html, username)
        image_URI = self._get_image_url(parsed_html)
        short_desc = self._get_short_desc(parsed_html)
        followers = self._get_followers(parsed_html, username)

        return TwitterProfile(name, image_URI, followers, short_desc)

    def _get_name(self, parsed_html, username):
        matching_html_tags = parsed_html.find_all('a', {'class': 'ProfileHeaderCard-nameLink'})
        return self.filter_lower_username(matching_html_tags, '/{0}'.format(username)).string

    def _get_image_url(self, parsed_html):
        image_tag = parsed_html.find('img', {'class': 'ProfileAvatar-image '})
        return image_tag['src'] if image_tag else '' #user hasn't profile picture

    def _get_short_desc(self, parsed_html):
        h = html2text.HTML2Text()
        h.ignore_links = True
        return h.handle(str(parsed_html.find('p', {'class': 'ProfileHeaderCard-bio'}))).rstrip()

    def _get_followers(self, parsed_html, username):
        matching_html_tags = parsed_html.find_all('a', {'class': 'ProfileNav-stat'})
        return self.filter_lower_username(matching_html_tags, '/{0}/followers'.format(username)).contents[5]['data-count']

    def filter_lower_username(self, iterable_html, comparison):
        return next(filter(lambda x: x.get('href', "").lower() == comparison, iterable_html))

class AccountSuspendedError(Exception):
    message = "This account has been suspended"
        