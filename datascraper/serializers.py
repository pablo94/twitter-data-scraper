from rest_framework import serializers

class TwitterProfileSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    short_desc = serializers.CharField(max_length=200)
    image_URI = serializers.URLField()
    popularity_index = serializers.IntegerField(min_value=0)