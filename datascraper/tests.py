from django.test import SimpleTestCase
from rest_framework.response import Response
from django.apps import apps
from .views import TwitterProfileViewSet
from .datascrapers import TwitterDataScraper, TwitterDataParser, AccountSuspendedError
from .models import TwitterProfile

config = apps.get_app_config('datascraper')

class FakeRequest():
    pass

class TwitterProfileViewSetTest(SimpleTestCase):
    def setUp(self):
        self.test_object = TwitterProfileViewSet()

    def test_twitter_profile_view_set_list(self):
        self.assertTrue(type(self.test_object.list(FakeRequest())) is Response)
        self.assertTrue(self.test_object.list(FakeRequest()).status_code == 200)

    def test_twitter_profile_view_set_retrieve(self):
        self.assertTrue(self.test_object.retrieve(FakeRequest(), 'test').status_code == 200)

class TwitterDataScraperTest(SimpleTestCase):
    fake_html_suspended = '<link href="https://twitter.com/account/suspended" rel="canonical"/>'

    def setUp(self):
        self.test_object = TwitterDataScraper()
        self.fake_html_complete_profile = self.complete_profile(self.lnamelink, self.short_desc, self.followers)
        self.fake_html_complete_profile_upper = self.complete_profile(self.unamelink, self.short_desc, self.ufollowers)
        self.fake_html_no_image = '{0} {1} {2}'.format(self.lnamelink(), self.short_desc(), self.followers())
        self.fake_html_no_desc = self.complete_profile(self.lnamelink, self.empty_desc, self.followers)

    def lnamelink(self):
        return self.namelink("test")

    def unamelink(self):
        return self.namelink("TesT")

    def complete_profile(self, name, desc, followers):
        return '{0} {1} {2} {3}'.format(name(), self.image(), desc(), followers())

    def namelink(self, name):
        return '<a class="ProfileHeaderCard-nameLink" href="/{0}">testname</a>'.format(name)

    def image(self):
        return '<img class="ProfileAvatar-image " src="http://im.age">'

    def short_desc(self):
        return '<p class="ProfileHeaderCard-bio">I am a test</p>'

    def empty_desc(self):
        return '<p class="ProfileHeaderCard-bio"></p>'

    def followers(self):
        return '<a class="ProfileNav-stat" href="/test/followers"> <span></span>\n<span></span>\n<span data-count="10"></span></a>'

    def ufollowers(self):
        return '<a class="ProfileNav-stat" href="/TesT/followers"> <span></span>\n<span></span>\n<span data-count="10"></span></a>'


    def test_twitter_data_scraper_get_by_username_account_suspended(self):
        try:
            self.test_object._validate_account(TwitterDataParser.parse(self.fake_html_suspended))
            self.assertTrue(False)
        except AccountSuspendedError as e:
            pass

    def test_twitter_data_scraper_get_by_username_complete_account(self):
        result = self.test_object.get_by_username('test', TwitterDataParser.parse(self.fake_html_complete_profile))

        self.assertEquals(TwitterProfile, type(result))
        self.assertEquals("testname", result.name)
        self.assertEquals("http://im.age", result.image_URI)
        self.assertEquals('10', result.popularity_index)
        self.assertEquals("I am a test", result.short_desc)


    def test_twitter_data_scraper_get_by_username_complete_account_links_has_upper(self):
        result = self.test_object.get_by_username('TesT', TwitterDataParser.parse(self.fake_html_complete_profile_upper))

        self.assertEquals(TwitterProfile, type(result))
        self.assertEquals("testname", result.name)
        self.assertEquals("http://im.age", result.image_URI)
        self.assertEquals('10', result.popularity_index)
        self.assertEquals("I am a test", result.short_desc)

    def test_twitter_data_scraper_get_by_username_no_image(self):
        result = self.test_object.get_by_username('test', TwitterDataParser.parse(self.fake_html_no_image))

        self.assertEquals("", result.image_URI)

    def test_twitter_data_scraper_get_by_username_no_desc(self):
        result = self.test_object.get_by_username('test', TwitterDataParser.parse(self.fake_html_no_desc))

        self.assertEquals("", result.short_desc)