class TwitterProfile(object):
    def __init__(self, name, image_URI, popularity_index, short_desc=""):
        super(TwitterProfile, self).__init__()
        self.name = name
        self.image_URI = image_URI
        self.popularity_index = popularity_index
        self.short_desc = short_desc
        