**Use python 3.x**

**Dependencies:**
- Django REST framework: http://www.django-rest-framework.org/#installation
- BeautifulSoup4: pip3 install beautifulsoup4
- urllib2: pip3 install urllib2
- html2text: pip3 install html2text